package api_tests;

import api_model.User;
import org.testng.Assert;
import org.testng.annotations.Test;
import retrofit2.Call;
import retrofit2.Response;

import java.util.List;

public class UserTests extends BaseTest{

    @Test
    public void getUserAPITest() {
        Call<User> userCall = getServiceManager()
                .getUserService()
                .getUser("mojombo");

        Response<User> response = execute(userCall);
        User user = response.body();

        Assert.assertEquals(response.code(), 200);
        Assert.assertEquals(user.getLogin(), "mojombo");
        Assert.assertEquals(user.getId(), 1);
        Assert.assertEquals(user.getUrl(), "https://api.github.com/users/mojombo");

        System.out.println(response);
    }

    @Test
    public void getUsersAPITest() {
        Call<List<User>> usersCall = getServiceManager()
                .getUserService()
                .getUsers(15, 1);
        Response<List<User>> response = execute(usersCall);
        List<User> users = response.body();

        Assert.assertEquals(response.code(), 200);

        for(User user : users) {
            System.out.println(user.getLogin());
        }
    }
}
