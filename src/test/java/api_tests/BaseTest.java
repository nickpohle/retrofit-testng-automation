package api_tests;

import api_model.ServiceManager;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseTest {
    Retrofit retrofit;
    ServiceManager serviceManager;

    public BaseTest(){
        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        serviceManager = new ServiceManager(retrofit);
    }

    public ServiceManager getServiceManager() {
        return serviceManager;
    }

    public <T> Response<T> execute(Call<T> call) {
        try {
            return call.execute();
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return null;
    }
}