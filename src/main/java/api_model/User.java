package api_model;

public class User {
    private String login;
    private long id;
    private String url;

    public String getLogin() {
        return login;
    }

    public String getUrl() {
        return url;
    }

    public long getId() {
        return id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
