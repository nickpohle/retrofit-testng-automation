package api_model;

import retrofit2.Retrofit;

public class ServiceManager {
    Retrofit retrofit;
    UserService userService;

    public ServiceManager(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public UserService getUserService() {
        userService = retrofit.create(UserService.class);
        return userService;
    }
}
